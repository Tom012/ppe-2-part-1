<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="../index.php">Camping Mâcon</a>
    </div>
    <ul class="nav navbar-nav">
      <li><a href="info.php">Informations</a></li>
      <li><a href="camping.php">Trouver le camping</a></li>
      <li><a href="detail.php">Réserver</a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <?php if(!isset($_SESSION['idcli'])): ?>
      <li><a href="page/inscription.php"><span class="glyphicon glyphicon-user"></span> Inscription</a></li>
      <li>
        <!-- Formulaire de la page inscription  -->
        <a href="#" data-toggle="modal" data-target="#login-modal"><span class="glyphicon glyphicon-log-in"></span> Connexion</a>
        <div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
          <div class="modal-dialog">
            <!-- Formulaire de connexion -->
            <div class="loginmodal-container">
              <h1>Connexion à votre compte</h1><br>
              <form method="POST">
                <input type="text" name="user" placeholder="Adresse mail">
                <input type="password" name="pass" placeholder="Mot de passe">
                <input type="submit" name="login" class="login loginmodal-submit" value="Se connecter">
              </form>
              <div class="login-help">
                <a href="../page/inscription.php">S'incrire</a>
              </div>
            </div>
          </div>
        </div>
      </li>
      <?php else: ?>
      <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="page/profil.php"><span class="glyphicon glyphicon-user"></span> Profil</a></li>
      <li><a href="#"><span class="glyphicon glyphicon-log-out"></span> Deconnexion</a></li>
      <?php endif; ?>
    </ul>
  </div>
</nav>