<?php
include("bddcn.php");
// Connexion de l'utilisateur
if(isset($_POST["login"]))
{
  if(!empty($_POST["user"])&& !empty($_POST["pass"]))
  {
    $reqresult = $cnn->prepare('select * from client where mail = :mail');
    $reqresult->bindParam(':mail', $_POST["mail"], PDO::PARAM_STR);

    $reqresult->execute(array('mail' => $_POST['user']));
    $resultat = $reqresult->fetch();

    // Comparaison du pass envoyé via le formulaire
    if($_POST["pass"] == $resultat["motpasse"]) 
    {
      $_SESSION['id'] = $resultat['idcli'];
      $_SESSION['nom'] = $resultat["nomCli"];
      $_SESSION['adresse'] = $resultat["adresse"];
      $_SESSION['cp'] = $resultat["cp"];
      $_SESSION['ville'] = $resultat["ville"];
      $_SESSION['telephone'] = $resultat["telephone"];

      // Rafraichissement de la page actuelle
      header('Location: '.$_SERVER['REQUEST_URI']);
    }
    else 
    {
      echo("<div id='dialog' title='Basic dialog'><p>Nom d'utilisateur ou mot de passe incorrect !</p></div>");
    }
    $reqresult->closeCursor();
  }
}
include("bddcls.php");
?>