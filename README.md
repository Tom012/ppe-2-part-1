# PPE 2 Part 1

Projet (PPE) réalisé en groupe lors de mes études en BTS.

### Contexte du projet

Aux portes du pays Mâconnais, le camping municipal de Mâcon, adhérent à la charte « Camping Qualité », est situé dans un environnement verdoyant, au bord de la Saône : 254 emplacements sur 5 hectares de terrain plat et ombragé, piscine chauffée, aires de jeux et de sports, salle de loisirs, magasin, bar, restaurant et bien d’autres services. A proximité immédiate : centre nautique avec 5 bassins, berges de la Saône (pêche, activités nautiques, port de plaisance), immense espace vert de détente sportive et de promenade.
Le camping municipal prévoit une extension de 40 mobil-homes qui seront mis en location au cours de l’année à venir.
Le projet vise à mettre en place une solution applicative, pour permettre d’assurer la gestion du parc de mobil-homes et de sa location auprès de la clientèle.
La société de services en informatique à laquelle vous appartenez a été retenue en tant que prestataire de services pour développer les applications métier du camping.

### Objectifs à réaliser

La solution applicative à mettre en place sera composée :
- D’une base de données centralisée Mysql contenant les données de gestion de l’activité du camping.
- D'une application backoffice déployée sur les postes du bureau d'accueil du camping permettant à chaque personne habilitée d’administrer les données du camping dans la base Mysql. Cette application sera développée avec Microsoft Visual Studio .Net en langage C#.
- D’un site internet frontoffice développé en Html,Css et Php avec accès dynamique aux données contenues dans la base de données Mysql permettant aux clients de consulter le parc de mobil-home, les disponibilités et la réservation sur Internet.

### Groupe

- Thomas MOREL
- Alexis MORIN

### Projet

[Partie 2](https://gitlab.com/Tom012/ppe-2-part-2)
