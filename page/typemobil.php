<?php
	include("../inc/header.php");
    include("../inc/navbar.php");
	include("../inc/bddcn.php");

// Affichage des boutons de selection du nombre de personnes.
	echo("
	&nbsp;
	Rechercher par nombre de personnes :
	<br/><br/>
	&nbsp;
	<span class='nbpertypmob'><a href='typemobil.php'>Afficher tout</a></span>
	&nbsp;
	<span class='nbpertypmob'><a href='typemobil.php?nbpers=2'>- 2</a></span>
	&nbsp;
	<span class='nbpertypmob'><a href='typemobil.php?nbpers=3'>&nbsp;3&nbsp;</a></span>
	&nbsp;
	<span class='nbpertypmob'><a href='typemobil.php?nbpers=4'>&nbsp;4&nbsp;</a></span>
	&nbsp;
	<span class='nbpertypmob'><a href='typemobil.php?nbpers=5'>5 +</a></span>
	<br/><br/>");

// Affichage des types de mobil homes.
			$reqresult = $cnn->prepare("select * from typemobil");
			$reqresult->execute();
			$uneligne = $reqresult->fetch();

			if (isset($_GET["nbpers"]))
			{

// Si le nombre des personnes <= 2.
				if($_GET["nbpers"]==2)
				{
					$reqresult = $cnn->prepare("select * from typemobil where nbpers<=:nbpers");
					$reqresult->bindParam(':nbpers',$_GET["nbpers"],PDO::PARAM_INT);
				}

// Si le nombre des personnes = 3 ou = 4.
				if($_GET["nbpers"]==3 || $_GET["nbpers"]==4)
				{
					$reqresult = $cnn->prepare("select * from typemobil where nbpers=:nbpers");
					$reqresult->bindParam(':nbpers',$_GET["nbpers"],PDO::PARAM_INT);
				}

// Si le nombre des personnes >= 5.
				if($_GET["nbpers"]==5)
				{
					$reqresult = $cnn->prepare("select * from typemobil where nbpers>=:nbpers");
					$reqresult->bindParam(':nbpers',$_GET["nbpers"],PDO::PARAM_INT);
				}
			}

// Sinon, affiche tout les mobil homes.
			else
			{
				$reqresult = $cnn->prepare("select * from typemobil");
			}

// Affiche les mobil homes dans le tableau.
			$reqresult->execute();
			echo("<table id=typmob><th>Nom</th><th>Informations</th><th>Tarif par jour</th>");
			$uneligne = $reqresult->fetch();
			while ($uneligne!=null)
			{
				$tarifj = $uneligne["tarifsemaine"]/7;
				echo("<tr><td><a href='detail.php?notypmob=$uneligne[idtyp]'>$uneligne[libtyp]</a></td><td>$uneligne[descripcourte]<br/>Pour $uneligne[nbpers] personnes</td><td>$tarifj €</td></tr>");
				$uneligne = $reqresult->fetch();
			}
			echo("</table>");
			$reqresult->closeCursor();

	include("../inc/bddcls.php");
	include("../inc/footer.php");
?>