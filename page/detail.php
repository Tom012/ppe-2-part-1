<?php
    include("../inc/header.php");
    include("../inc/navbar.php");
    include("../inc/bddcn.php");

// Condition : Un mobilhome doit etre selectionner depuis la page précédente "typemobil.php".
            if(isset($_GET["notypmob"]))
            {

// Requête : affichage des informations du type de mobilhome selectionne.
                $reqresult = $cnn->prepare("select * from typemobil join photo on photo.idtyp=typemobil.idtyp where typemobil.idtyp=:notypmob");
                $reqresult->bindParam(':notypmob',$_GET["notypmob"],PDO::PARAM_INT);
                $reqresult->execute();
                $uneligne = $reqresult->fetch();
                $tarifj = $uneligne["tarifsemaine"]/7;
                $descriplongue = $uneligne["descriplongue"];

// Affichage des donnees depuis la base de donnees.
                echo("
        <h2>$uneligne[libtyp]</h2>
        <div class='col-md-4 mb-md-30'>");

// Affichage des images.
                $reqresult = $cnn->prepare ("select * from photo where idtyp=:notypmob limit 1,1");
                $reqresult->bindParam(':notypmob',$_GET["notypmob"],PDO::PARAM_INT);
                $reqresult->execute();
                $uneligne = $reqresult->fetch();
                while($uneligne != null)
                {
                    echo("
            <div class='post-prev-img'>
                <a href='../img/$uneligne[nomfichier]'><img src='../img/$uneligne[nomfichier]'/></a>
            </div>");
                  $uneligne = $reqresult->fetch();
                }
                $reqresult->closeCursor();
                echo("
            <div class='row'>");
                $reqresult = $cnn->prepare ("select * from photo where idtyp=:notypmob");
                $reqresult->bindParam(':notypmob',$_GET["notypmob"],PDO::PARAM_INT);
                $reqresult->execute();
                $uneligne = $reqresult->fetch();
                while($uneligne != null)
                {
                    echo("
                <div class='col-xs-3 post-prev-img'>
                    <a href='../img/$uneligne[nomfichier]' class='lightbox-gallery-3 mfp-image'><img src='../img/$uneligne[nomfichier]'/></a>
                </div>");
                  $uneligne = $reqresult->fetch();
                }
                $reqresult->closeCursor();

// Affichage des details du mobilhome.
                echo("
            </div>
        </div>
        <br/>
        <br/>
        <table id='detail'>
            <th>Description</th>
            <th>Prix</th>
            <tr>
                <td>$descriplongue</td>
                <td>$tarifj € par jour</td>
            </tr>
        </table>
        <br/>");
                $reqresult->closeCursor();
                echo("
        <h2>Disponibilitée et réservation</h2>
        <h3>Selectionner un mobilhome : </h3>
        <form method='get' action='detail.php'>
            <input type='hidden' name='notypmob' readonly value='$_GET[notypmob]'/>
            <h2><select name='cbomobilhome' onchange='form.submit()'>");

// Requete : affichage des mobilhomes du type dans un combo box et garde le mobilhome selectionne.
                $reqresult = $cnn->prepare("select * from mobilhome where idtyp=:notypmob");
                $reqresult->bindParam(':notypmob',$_GET["notypmob"],PDO::PARAM_INT);
                $reqresult->execute();
                $uneligne = $reqresult->fetch();
                while($uneligne != null)
                {
                    if(isset($_GET["cbomobilhome"]) && $_GET["cbomobilhome"] == $uneligne["idmob"])
                    {
                        echo("
                    <option value='$uneligne[idmob]' selected>$uneligne[nom]</option>");
                        $uneligne = $reqresult->fetch();
                    }
                    else
                    {
                        echo("
                    <option value='$uneligne[idmob]'>$uneligne[nom]</option>");
                        $uneligne = $reqresult->fetch();
                    }
                }
                $reqresult->closeCursor();
                echo("
            </select></h2>
        </form>");

// Affichage des disponibilites du mobilhome selectionner dans le combo box.
                $nbmois = 6;
                $datej = date("d M Y");

                if(isset($_GET["cbomobilhome"]))
                {
                    echo("
        <h3>Disponibilitée du mobilhome : </h3>
        <table id='cal'>");

// Bloucle d'affichage du tableau des mois.
                    for($i=1;$i<$nbmois;$i++)
                    {
                        $moisd = date("M Y", mktime(0, 0, 0, date("m")+$i, 0, date("Y")));
                        echo("
            <th>$moisd</th>");
                    }
                    echo("
            <tr>");

// Boucle d'affichage des jours dans chaque mois.
                    for($i=1;$i<$nbmois;$i++)
                    {
                        $moisres = date("Y-m", mktime(0, 0, 0, date("m")+$i, 0, date("Y")));
                        $moisw = str_replace("0","7",date("w", mktime(0, 0, 0, date("m")+$i-1, 0, date("Y"))));
                        $moisj = date("t", mktime(0, 0, 0, date("m")+$i, 0, date("Y")));
                        echo("
                <td>
                    <table id='date'>
                        <th>Lu</th>
                        <th>Ma</th>
                        <th>Me</th>
                        <th>Je</th>
                        <th>Ve</th>
                        <th>Sa</th>
                        <th>Di</th>
                        <tr>");

// Condition : Remplissage des colonnes vides dans le tableau des jours si il y en a.
                        if ($moisw != 0 && $moisw < 7)
                        {
                            echo ("
                            <td colspan=$moisw></td>");
                        }

// Affichage des jours dans le tableau.
                        for($j=1;$j<=$moisj;$j++)
                        {
                            if($j<10)
                            {
                                $checkres = $moisres.'-0'.$j;
                            }
                            else
                            {
                                $checkres = $moisres.'-'.$j;
                            }

// Comparaison de chaque jour pour vérifier si une reservation existe deja.
                            $reqresult = $cnn->prepare("select * from reservation where idmob=:mobilhome and '$checkres' between datedebut and datefin");
                            $reqresult->bindParam(':mobilhome',$_GET["cbomobilhome"],PDO::PARAM_INT);
                            $reqresult->execute();
                            $uneligne = $reqresult->fetch();

// Affichage des jours en rouge si il y a une reseravation.
                            if ($uneligne)
                            {
                                echo("
                            <td class='ndisp'>$j</td>");
                            }

// Sinon, affichage des jours en vert.
                            else
                            {
                                echo("
                            <td class='disp'>$j</td>");
                            }

// Condition : Change de ligne dans le tableau si la précedente est complete.
                            if($moisw+$j==7||$moisw+$j==14||$moisw+$j==21||$moisw+$j==28||$moisw+$j==35)
                            {
                            echo("
                        </tr>
                        <tr>");
                            }
                        }
                        $reqresult->closeCursor();
                        echo("
                        </tr>
                    </table>
                </td>");
                    }
                }
                echo("
            </tr>
        </table>
            ");

// Reservation du mobil home selectionne sur la page de profil quand l'utilisateur est connecte.
                if(isset($_SESSION["idcli"]) && isset($_GET["cbomobilhome"]))
                {
                    $_SESSION["idmob"]=$_GET["cbomobilhome"];
                    echo("<a href='profil.php'><h2>Réserver ce mobil home</h2></a>");
                }

// Sinon, L'utilisateur doit se connecter et recommencer.
                else
                {
                    if(isset($_SESSION["idcli"]))
                    {
                        echo("<h2>Sélectionner un mobil home pour le réserver</h2>");
                    }
                    else
                    {
                        echo("<h2>Connectez-vous pour réserver un mobil home</h2>");
                    }
                }
            }
            else{
                echo("
        <br/>
        <br/>
        <a href='typemobil.php'><h2>Sélectionnez un mobilhome pour consulter les détails</h2></a>");
            }
    include("../inc/bddcls.php");
    include("../inc/footer.php");
?>