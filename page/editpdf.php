<?php
    require("../fpdf181/fpdf.php");
    $pdf = new FPDF();
    $pdf->AddPage();
    $pdf->SetFont("Arial","B",16);
    $pdf->Text(20,20, "Camping Mâcon");
    $pdf->SetFont("Arial","B",12);
    
    // Entête de la facture PDF 
    $pdf->Text(120,50,"$_SESSION[nom]");
    $pdf->Text(120,60,"$_SESSION[adresse]");
    $pdf->Text(120,70,"$_SESSION[cp]");
    $pdf->Text(120,80,"$_SESSION[ville]");
    $pdf->Text(120,90,"$_SESSION[telephone]");

    // Détail de la facture PDF 
    $poslig = 90;
    $pdf->Text(20,$poslig, "N°");
    $pdf->Text(40,$poslig, "Mobil home");
    $pdf->Text(100,$poslig, "Réservé le");
    $pdf->Text(120,$poslig, "Du");
    $pdf->Text(140,$poslig, "Au");

    $pdf->Text(20,$poslig, "$_GET[idres]");
    $pdf->Text(40,$poslig, "$_GET[idmob]");
    $pdf->Text(100,$poslig, "$_GET[dateres]");
    $pdf->Text(120,$poslig, "$_GET[datedeb]");
    $pdf->Text(140,$poslig, "$_GET[datefin]");
?>