<!-- Insertion du haut de page -->
<?php
	include("../inc/header.php");
	include("../inc/navbar.php");
?>
	<script> $( function() { $( "#dialog" ).dialog(); } ); </script>
	<div class="container">
		<!-- Formulaire de la page inscription  -->
		<form method="POST" action="inscription.php">
			Formulaire d'inscription <br/>
			<table>
				<tr>
					<td>NOM</td><td><input type="text" name="txtnomcli"/></td>
				</tr>
				<tr>
					<td>Prenom</td><td><input type="text" name="txtprecli"/></td>
				</tr>
				<tr>
					<td>Mot de passe</td><td><input type="text" name="txtpass"/></td>
				</tr>
				<tr>
					<td>Adresse</td><td><input type="text" name="txtadresse"/></td>
				</tr>
				<tr>
					<td>Code Postal</td><td><input type="text" name="txtcodep"/></td>
				</tr>
				<tr>
					<td>Ville</td><td><input type="text" name="txtville"/></td>
				</tr>
				<tr>
					<td>Téléphone</td><td><input type="text" name="txttel"/></td>
				</tr>
				<tr>
					<td>Mail</td><td><input type="text" name="txtmail"/></td>
				</tr>
			</table>
			<input type="submit" name="btninscription" value="Inscription" />
		</form>
	</div>
	<!-- Traitement de l'inscription -->
<?php
// Ajout des données utilisateur dans la base de données.
	if(isset($_POST["btninscription"]))
	{
		if(!empty($_POST["txtnomcli"])&& !empty($_POST["txtprecli"])&& !empty($_POST["txtpass"])&& !empty($_POST["txtadresse"])&& !empty($_POST["txtcodep"])&& !empty($_POST["txtville"])&& !empty($_POST["txttel"])&& !empty($_POST["txtmail"]))
		{
// Concaténation du NOM et du PRENOM.
			$nomprecli = ($_POST["txtnomcli"] . " " . $_POST["txtprecli"]);

// Connexion à la base de données.
			$cnn = new PDO("mysql:host=localhost;dbname=campingmorel-morin;charset=utf8", "root", "");
			$reqresult = $cnn->prepare("insert into client(nomcli, adresse, cp, ville, telephone, mail, motpasse) values(:nom, :adresse, :cp, :ville, :tel, :mail, :mdp)");

// Attribution des type aux données récupérés.
			$reqresult->bindParam(':nom',$nomprecli, PDO::PARAM_STR);
			$reqresult->bindParam(':adresse',$_POST["txtadresse"], PDO::PARAM_STR);
			$reqresult->bindParam(':cp',$_POST["txtcodep"], PDO::PARAM_STR);
			$reqresult->bindParam(':ville',$_POST["txtville"], PDO::PARAM_STR);
			$reqresult->bindParam(':tel',$_POST["txttel"], PDO::PARAM_STR);
			$reqresult->bindParam(':mail',$_POST["txtmail"], PDO::PARAM_STR);
			$reqresult->bindParam(':mdp',$_POST["txtpass"], PDO::PARAM_STR);
			
// Execution de la requête.
			$ok = $reqresult->execute();
			if ($ok == true)
			{
				echo("<div id='dialog' title='Basic dialog'><p>Inscription effectué avec succès.</p></div>");
			}
			else
			{
				echo("<div id='dialog' title='Basic dialog'><p>Echec, inscription non effectué.</p></div>");
			}
			$cnn=null;
		}
		else
		{
			echo("<div id='dialog' title='Basic dialog'><p>Un ou plusieurs champs n'ont pas été remplis !</p></div>");
		}
	}
?>
<!-- Insertion du bas de page -->
<?php include("../inc/footer.php"); ?>