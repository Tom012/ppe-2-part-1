<!-- Insertion du haut de page -->
<?php
	include("../inc/header.php");
	include("../inc/bddcn.php");
	include("../inc/navbar.php");
?>
<script> $( function() {$( "#datepickerdeb" ).datepicker();} );</script>
<script> $( function() {$( "#datepickerfin" ).datepicker();} );</script>
		<div class="container">
	<!-- <?php $_SESSION['idcli']=1; ?> -->
			<!-- Formulaire de la page profil  -->
			<form method="POST" action="profil.php">
				<input type="submit" name="btnres" value="Historique des réservations" />
				<input type="submit" name="btnaddres" value="Ajouter une réservation" />
			</form>
	<?php

// Affichage des reservations de l'utilisateur.
		if(isset($_POST["btnres"]))
		{
			$reqresult = $cnn->prepare("select * from reservation where idcli=".$_SESSION["idcli"]);
			$reqresult->execute();
			echo ("
			<table id='reservation'>
				<th>N°</th>
				<th>Mobilhome</th>
				<th>Date de réservation</th>
				<th>Début</th>
				<th>Fin</th>
				<th>Réglement</th>
				<th>Supprimer la réservation</th>
				<th>Facture PDF</th>");
			while ($row = $reqresult->fetch())
			{
				echo("
				<tr>
					<td>$row[idres]</td>
					<td>$row[idmob]</td>
					<td>$row[dateres]</td>
					<td>$row[datedebut]</td>
					<td>$row[datefin]</td>");
				if($row["regleon"]==1)
				{
					echo "<td>Réglé</td>";
				}
				else
				{
					echo "<td>Non réglé</td>";
				}
				echo "<td><a href='suppression.php?id=$row[idres]'>Annuler / Supprimer</a></td>";
				echo "<td><a href='editpdf.php?idres=$row[idres]&idmob=$row[idmob]&dateres=$row[dateres]&datedeb=$row[datedebut]&datefin=$row[datefin]'>PDF</a></td>";
			}
			echo("
				</tr></table>");
			$reqresult->closeCursor();
		}

//  Nouvelle reservations de l'utilisateur si il est connecte.
		if(isset($_POST["btnaddres"]))
		{

// Et si il a selectionne un mobil home, affichage du formulaire.
			if(isset($_SESSION["idmob"]))
			{
				echo("
			<form method='POST' action='profil.php'>
				<table>
					<tr>
						<td>Entre le </td><td><input type='text' id='datepickerdeb'></td>
					</tr>
					<tr>
						<td>Et le </td><td><input type='text' id='datepickerfin'></td>
					</tr>
					<tr>
						<td>Règlement</td><td><label><input type='radio' name='reglement' value='1'>Effectué</label>
					</tr>
					<tr>
						<td></td><td><label><input type='radio' name='reglement' value='0'>Non effectué</label></td>
					</tr>
				</table>
				<input type='hidden' name='nomob' readonly value='$_SESSION[idmob]'/>
				<input type='submit' name='btnresok' value='Valider la réservation'/>
			</form>");

// verification et ajout de la reservation dans la base de donnees.
				if(isset($_POST["btnresok"]))
				{
					$reqresult = $cnn->prepare("select * from reservation where idmob=:mobilhome and '$_POST[datedebut]'>=datedebut and '$_POST[datedebut]'<datefin or '$_POST[datefin]'>=datedebut and '$_POST[datefin]'<datefin");
					$reqresult->bindParam(':mobilhome',$_POST["nomob"],PDO::PARAM_INT);
					$reqresult->execute();
// FINIR		Verifier si le mobilhome est deja reserver sur cette periode.

// Si la periode est disponible, insertion dans la base de donnees.
					if($dateok == true)
					{
	  					$reqresult = $cnn->prepare("insert into reservation(dateres, datedebut, datefin, reglon, idmob, idcli) values (:dateres,:datedebut,:datefin,:reglon,:idmob,:idcli);");
						$reqresult->bindParam(':dateres',date("Y-m-d"),PDO::PARAM_STR);
						$reqresult->bindParam(':datedebut',$_POST["datepickerdeb"],PDO::PARAM_STR);
						$reqresult->bindParam(':datefin',$_POST["datefin"],PDO::PARAM_STR);
						$reqresult->bindParam(':reglon',$_POST["reglement"],PDO::PARAM_INT);
						$reqresult->bindParam(':idmob',$_POST["nomob"],PDO::PARAM_INT);
						$reqresult->bindParam(':idcli',$_SESSION["idcli"],PDO::PARAM_INT);
						$ok = $reqresult->execute();
						if ($ok == true)
							echo("Réservation effectué avec succès");
						else
							echo("Echec, réservation non effectué");
					}

// Sinon, echec de l'ajout.
					else
					{
						echo("Echec, réservation non effectué");
					}
				}
			}

// Sinon, il doit selectionner un mobil home sur la page des mobil homes.
			else
			{
				echo("<a href='typemobil.php'>Choisir un mobil home</a>");
			}
		}
	?>
		</div>
		<!-- Insertion du bas de page -->
<?php
	include("../inc/bddcls.php");
	include("../inc/footer.php");
?>